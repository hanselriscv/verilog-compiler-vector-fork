# Goal

A verily macro preprocessor, we are designing our own marco to support our RISC-V cpu development

# Macro

Auto wire two modules, need to base on the wire name standard

@{connect(moduleA, module B)}

Generate code to dump pin 1 information into our own trace format file

@{dump(moduleA.pin1)}

# CLI Usage

```
usage: java -jar assembler-xx.jar [OPTION] <input file>
 -c,--compile <.v file>                   compile verilog file
 -h,--help                                help
 -j,--json <.v file>                      verilog to json
 -p,--json <folder, top module .v file>   parse topmodule and return json
 -v,--version                             display version
```

## java -jar verilogcompiler-1.0.jar -p .,quantr_i.v

Result:

```
{
  "name" : "quantr_i",
  "inputs" : [ {
    "direction" : "input",
    "name" : "clk"
  }, {
    "direction" : "input",
    "name" : "rst"
  } ],
  "outputs" : [ {
    "direction" : "output",
    "range" : "[`MXLEN]",
    "name" : "pc"
  } ],
  "wires" : {
    "IMM_I" : {
      "name" : "IMM_I",
      "type" : "[`MXLEN]",
      "port1Module" : {
        "name" : "id",
        "inputs" : [ {
          "direction" : "input",
          "type" : "wire",
          "name" : "rst"

```

## java -jar verilogcompiler-1.0.jar -j quantr_i.v

Result:

```
[ {
  "name" : "quantr_i",
  "inputs" : [ {
    "direction" : "input",
    "name" : "clk"
  }, {
    "direction" : "input",
    "name" : "rst"
  } ],
  "outputs" : [ {
    "direction" : "output",
    "range" : "[`MXLEN]",
    "name" : "pc"
  } ]
} ]
```
