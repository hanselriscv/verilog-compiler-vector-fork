
module id_ex(input wire clk,
             input wire rst,
             input wire[`AluOpBus] id_aluop_i,
             input wire[`AluSelBus] id_alusel_i,
             input wire[`MXLEN] id_reg1_i,
             input wire[`MXLEN] id_reg2_i,
             input wire[`REG_WIDTH] id_wd_i,
             input wire id_wreg_i,
             output reg[`AluOpBus] ex_aluop_o,
             output reg[`AluSelBus] ex_alusel_o,
             output reg[`MXLEN] ex_reg1_o,
             output reg[`MXLEN] ex_reg2_o,
             output reg[`REG_WIDTH] ex_wd_o,
             output reg ex_wreg_o);
    
    always @ (posedge clk) begin
        if (rst == `RESET) begin
            ex_aluop_o  <= `EXE_NOP_OP;
            ex_alusel_o <= `EXE_RES_NOP;
            ex_reg1_o   <= `ZeroDWord;
            ex_reg2_o   <= `ZeroDWord;
            ex_wd_o     <= 5'b0;
            ex_wreg_o   <= `WriteDisable;
            end else begin
            ex_aluop_o  <= id_aluop_i;
            ex_alusel_o <= id_alusel_i;
            ex_reg1_o   <= id_reg1_i;
            ex_reg2_o   <= id_reg2_i;
            ex_wd_o     <= id_wd_i;
            ex_wreg_o   <= id_wreg_i;
        end
    end
endmodule
    
