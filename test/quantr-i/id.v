/* verilator lint_off UNUSED */
module id(input wire rst,
        //   input wire[`MXLEN] pc_i,
          input wire[`INST_WIDTH] inst_i,
          input wire[`MXLEN] reg1_data_i,
          input wire[`MXLEN] reg2_data_i,
          output reg reg1_read_o,
          output reg reg2_read_o,
          output reg[`REG_WIDTH] reg1_addr_o,
          output reg[`REG_WIDTH] reg2_addr_o,
          output reg[`AluOpBus] aluop_o,
          output reg[`AluSelBus] alusel_o,
          output reg[`MXLEN] reg1_o,
          output reg[`MXLEN] reg2_o,
          output reg[`REG_WIDTH] wd_o,
          output reg wreg_o);

    wire[6:0] opcode = inst_i[6:0];
    wire[2:0] funct3 = inst_i[14:12];
    
    reg[`MXLEN] imm;
    reg instvalid;
    
    always @(*) begin
        if (rst == `RESET) begin
            aluop_o     = 8'b00000000;
            alusel_o    = 3'b000;
            wd_o        = 5'b00000;
            wreg_o      = 1'b0;
            instvalid   = 1'b0;
            reg1_read_o = 1'b0;
            reg2_read_o = 1'b0;
            reg1_addr_o = 5'b00000;
            reg2_addr_o = 5'b00000;
            imm         = `ZeroDWord;
            end else begin
			aluop_o     = 8'b00000000;
			alusel_o    = 3'b000;
			wd_o        = 5'b00000;
			wreg_o      = 1'b0;
			instvalid   = 1'b1;
			reg1_read_o = 1'b0;
			reg2_read_o = 1'b0;
			reg1_addr_o = 5'b00000;
			reg2_addr_o = 5'b00000;
			imm         = `ZeroDWord;

			if (opcode == 7'b0110011 && funct3 == 3'd6) begin
				wreg_o      = 1'b1;
				aluop_o     = `EXE_OR_OP;
				alusel_o    = `EXE_RES_LOGIC;
				reg1_read_o = 1'b1;
				reg2_read_o = 1'b0;
				imm         = {48'h0, inst_i[15:0]};
				wd_o        = inst_i[20:16];
				instvalid   = 1'b0;
			end
		end
    end
    
    always @(*) begin
        if (rst == `RESET) begin
            reg1_o = `ZeroDWord;
            end else if (reg1_read_o == 1'b1) begin
            reg1_o = reg1_data_i;
            end else if (reg1_read_o == 1'b0) begin
            reg1_o = imm;
            end else begin
            reg1_o = `ZeroDWord;
        end
    end
    
    always @(*) begin
        if (rst == `RESET) begin
            reg2_o = `ZeroDWord;
            end else if (reg2_read_o == 1'b1) begin
            reg2_o = reg2_data_i;
            end else if (reg2_read_o == 1'b0) begin
            reg2_o = imm;
            end else begin
            reg2_o = `ZeroDWord;
        end
    end
endmodule
