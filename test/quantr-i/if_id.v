module if_id(input wire clk,
             input wire rst,
             input wire[`MXLEN] if_pc,
             input wire [`INST_WIDTH] if_inst,
             output reg[`MXLEN] id_pc,
             output reg[`INST_WIDTH] id_inst);
    always @(posedge clk) begin
        if (rst == `RESET) begin
            id_pc   <= `ZeroDWord;
            id_inst <= `ZeroWord;
            end else begin
            id_pc   <= if_pc;
            id_inst <= if_inst;
        end
    end
endmodule
