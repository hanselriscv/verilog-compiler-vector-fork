package hk.quantr.verilogcompiler;

import hk.quantr.verilogcompiler.listener.VerilogCompileListener;
import hk.quantr.verilogcompiler.listener.SingleModuleListener;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import hk.quantr.peterswing.CommonLib;
import hk.quantr.peterswing.PropertyUtil;
import hk.quantr.quantrverilogtool.antlr.Verilog2001Lexer;
import hk.quantr.quantrverilogtool.antlr.Verilog2001Parser;
import hk.quantr.quantrverilogtool.antlr.VerilogMacroLexer;
import hk.quantr.quantrverilogtool.antlr.VerilogMacroParser;
import hk.quantr.verilogcompiler.listener.AllModuleListener;
import hk.quantr.verilogcompiler.macro.Macro;
import hk.quantr.verilogcompiler.macro.VerilogPreprocessor;
import hk.quantr.verilogcompiler.structure.Module;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.atn.PredictionMode;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class VerilogCompiler {

	private static Logger logger = Logger.getLogger(VerilogCompiler.class.getName());

	public static void main(String args[]) throws ParseException, FileNotFoundException, IOException {
		Options options = new Options();
		options.addOption("v", "version", false, "display version");
		options.addOption(Option.builder("c")
				.required(false)
				.hasArg()
				.argName(".v file")
				.desc("compile verilog file")
				.longOpt("compile")
				.build());
		options.addOption(Option.builder("j")
				.required(false)
				.hasArg()
				.argName(".v file")
				.desc("verilog to json")
				.longOpt("json")
				.build());
		options.addOption(Option.builder("p")
				.required(false)
				.hasArg()
				.argName("folder, top module .v file")
				.desc("parse topmodule and return json")
				.longOpt("json")
				.build());
		options.addOption("h", "help", false, "help");

		if (Arrays.asList(args).contains("-h") || Arrays.asList(args).contains("--help")) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("java -jar assembler-xx.jar [OPTION] <input file>", options);
			return;
		}
		if (Arrays.asList(args).contains("-v") || Arrays.asList(args).contains("--version")) {
			System.out.println("version : " + PropertyUtil.getProperty("main.properties", "version"));

			TimeZone utc = TimeZone.getTimeZone("UTC");
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			format.setTimeZone(utc);
			Calendar cl = Calendar.getInstance();
			try {
				Date convertedDate = format.parse(PropertyUtil.getProperty("main.properties", "build.date"));
				cl.setTime(convertedDate);
				cl.add(Calendar.HOUR, 8);
			} catch (java.text.ParseException ex) {
				logger.log(Level.SEVERE, null, ex);
			}
			System.out.println("build date : " + format.format(cl.getTime()) + " HKT");
			return;
		}
		CommandLineParser cliParser = new DefaultParser();
		CommandLine cmd = cliParser.parse(options, args);
		List<String> arguments = cmd.getArgList();
		if (cmd.hasOption("j")) {
			ArrayList<Module> outputs = new ArrayList<>();
			String temp = cmd.getOptionValue("j");
			File file = new File(temp);
			if (!(file.isFile() && file.exists())) {
				logger.log(Level.SEVERE, "verilog file/folder " + file.getAbsolutePath() + " not exist");
				return;
			}

			String content = null;
			if (file.isFile()) {
				content = IOUtils.toString(new FileInputStream(file), "utf-8");
				outputs.add(parseVerilogForOutput(content));
			} else if (file.isDirectory()) {
				Collection<File> files = FileUtils.listFiles(file, new String[]{"v"}, true);
				for (File f : files) {
					outputs.add(parseVerilogForOutput(IOUtils.toString(new FileInputStream(f), "utf-8")));
				}
			}
			System.out.println(CommonLib.objectToJson(outputs));
		} else if (cmd.hasOption("c")) {
			String temp = cmd.getOptionValue("c");
			File verilogFile = new File(temp);
			if (!(verilogFile.isFile() && verilogFile.exists())) {
				logger.log(Level.SEVERE, "verilog file " + verilogFile.getAbsolutePath() + " not exist");
				return;
			}

			Verilog2001Lexer lexer = new Verilog2001Lexer(CharStreams.fromString(IOUtils.toString(new FileInputStream(verilogFile), "utf-8")));

			VerilogCompileListener listener = new VerilogCompileListener();

			for (Token token : lexer.getAllTokens()) {
				if (token.getChannel() == Verilog2001Lexer.HIDDEN && !token.getText().trim().equals("")) {
					int lineNo = token.getLine();
					int startIndex = token.getStartIndex();
					int stopIndex = token.getStopIndex();

					System.out.println(Verilog2001Lexer.VOCABULARY.getDisplayName(token.getType()) + " = " + Verilog2001Lexer.channelNames[token.getChannel()] + " = " + token.getText().replaceAll("\n", "") + " = " + token.getLine() + " , " + token.getStartIndex());
					String comment = token.getText();
					VerilogPreprocessor processor = new VerilogPreprocessor();
					ArrayList<Macro> list = processor.process(comment, lineNo, startIndex, stopIndex);
					for (Macro macro : list) {
						VerilogMacroLexer lexer2 = new VerilogMacroLexer(CharStreams.fromString(macro.command));
						CommonTokenStream tokenStream = new CommonTokenStream(lexer2);
						VerilogMacroParser parser2 = new VerilogMacroParser(tokenStream);
						parser2.addParseListener(listener);
						parser2.compile();
						System.out.println(macro);
					}
				}
			}
		} else if (cmd.hasOption("p")) {
			String temp[] = cmd.getOptionValue("p").split(",");
			File folder = new File(temp[0]);
			hk.quantr.verilogcompiler.structure.Module module = VerilogCompiler.parseTopModule(folder, temp[1]);
			System.out.println(CommonLib.objectToJson(module));
		}
	}

	public static Module parseVerilogForOutput(String content) {
		Verilog2001Lexer lexer = new Verilog2001Lexer(CharStreams.fromString(content));
		CommonTokenStream tokenStream = new CommonTokenStream(lexer);
		SingleModuleListener listener = new SingleModuleListener();
		Verilog2001Parser parser = new Verilog2001Parser(tokenStream);
		parser.addParseListener(listener);
		Verilog2001Parser.Source_textContext context = parser.source_text();

		Module output = new Module();
		output.name = listener.moduleName;
		output.inputs = listener.inputs;
		output.outputs = listener.outputs;
		return output;
	}

	public static Module parseTopModule(File folder, String topModuleFileName) {
		File topModuleFile = new File(folder.getAbsolutePath() + "/" + topModuleFileName);

		Collection<File> files = FileUtils.listFiles(folder, new String[]{"v"}, true);
		HashMap<String, hk.quantr.verilogcompiler.structure.Module> modules = new HashMap<>();
		for (File file : files) {
			try {
//				System.out.println("processing " + file.getAbsolutePath());

				Verilog2001Lexer lexer = new Verilog2001Lexer(CharStreams.fromString(IOUtils.toString(new FileInputStream(file), "utf-8")));
				CommonTokenStream tokenStream = new CommonTokenStream(lexer);
				SingleModuleListener listener = new SingleModuleListener();
				Verilog2001Parser parser = new Verilog2001Parser(tokenStream);
				parser.getInterpreter().setPredictionMode(PredictionMode.SLL);
				parser.addParseListener(listener);
				Verilog2001Parser.Source_textContext context = parser.source_text();

				if (listener.moduleName == null) {
					continue;
				}
				modules.put(listener.moduleName, new hk.quantr.verilogcompiler.structure.Module(listener.moduleName, listener.inputs, listener.outputs));
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}

		// parse top module
		try {
			Verilog2001Lexer lexer = new Verilog2001Lexer(CharStreams.fromString(IOUtils.toString(new FileInputStream(topModuleFile), "utf-8")));
			CommonTokenStream tokenStream = new CommonTokenStream(lexer);
			AllModuleListener listener = new AllModuleListener(modules);
			Verilog2001Parser parser = new Verilog2001Parser(tokenStream);
			parser.addParseListener(listener);
			Verilog2001Parser.Source_textContext context = parser.source_text();

			hk.quantr.verilogcompiler.structure.Module topModule = new hk.quantr.verilogcompiler.structure.Module();
			topModule.name = listener.moduleName;
			topModule.modules = listener.modules;
			topModule.inputs = listener.inputs;
			topModule.outputs = listener.outputs;
			topModule.wires = listener.wires;

//			System.out.println(topModule);
			return topModule;
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;
	}
}
