grammar VerilogMacro;

compile
	:	macro*
	;

macro
	:	'@{' macro_body '}'
	;

macro_body
	:	name = 'connect' '(' module_name (COMMA module_name)+ ')'
	;

module_name
	:	IDENTIFIER
	;

IDENTIFIER	:	[a-zA-Z0-9]+;

COMMA : ',';

WHITE_SPACE
   : [ \t\n\r] + -> channel (HIDDEN)
   ;
