/*
 * Quantr Commercial License
 */
package hk.quantr.verilogcompiler.macro;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author peter
 */
public class VerilogPreprocessor {

	public ArrayList<Macro> process(String comment, int lineNo, int startIndex, int stopIndex) {
		ArrayList<Macro> macros = new ArrayList<>();
		String str = "@\\{([^}]*)\\}";
		Pattern pattern = Pattern.compile(str);
		Matcher matcher = pattern.matcher(comment);
		while (matcher.find()) {
			String command = matcher.group(0);
//			System.out.println("command=" + command);
			macros.add(new Macro(lineNo, startIndex, stopIndex, command));
		}
		return macros;
	}
}
