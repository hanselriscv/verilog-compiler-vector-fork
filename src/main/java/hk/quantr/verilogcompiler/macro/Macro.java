/*
 * Quantr Commercial License
 */
package hk.quantr.verilogcompiler.macro;

/**
 *
 * @author peter
 */
public class Macro {

	public int lineNo;
	public int startIndex;
	public int stopIndex;
	public String command;

	public Macro(int lineNo, int startIndex, int stopIndex, String command) {
		this.lineNo = lineNo;
		this.startIndex = startIndex;
		this.stopIndex = stopIndex;
		this.command = command;
	}

	@Override
	public String toString() {
		return "Macro{" + "lineNo=" + lineNo + ", startIndex=" + startIndex + ", stopIndex=" + stopIndex + ", command=" + command + '}';
	}

}
