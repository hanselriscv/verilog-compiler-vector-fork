package hk.quantr.verilogcompiler.listener;

import hk.quantr.quantrverilogtool.antlr.VerilogMacroBaseListener;
import hk.quantr.quantrverilogtool.antlr.VerilogMacroParser;
import org.antlr.v4.runtime.ParserRuleContext;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class VerilogCompileListener extends VerilogMacroBaseListener {

	@Override
	public void exitMacro_body(VerilogMacroParser.Macro_bodyContext ctx) {
		System.out.println(ctx.name.getText() + " = " + ctx.module_name(0).getText());
	}
}
