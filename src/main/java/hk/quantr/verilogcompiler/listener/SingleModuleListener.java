package hk.quantr.verilogcompiler.listener;

import hk.quantr.quantrverilogtool.antlr.Verilog2001BaseListener;
import hk.quantr.quantrverilogtool.antlr.Verilog2001Parser;
import hk.quantr.quantrverilogtool.antlr.Verilog2001Parser.Port_identifierContext;
import hk.quantr.verilogcompiler.structure.Port;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class SingleModuleListener extends Verilog2001BaseListener {

	public ArrayList<Port> inputs = new ArrayList<>();
	public ArrayList<Port> outputs = new ArrayList<>();
	public String moduleName;

	@Override
	public void exitInput_declaration(Verilog2001Parser.Input_declarationContext ctx) {
		String type = ctx.net_type() == null ? null : ctx.net_type().getText();
		String range = ctx.range_() == null ? null : ctx.range_().getText();

		List<Port_identifierContext> list = ctx.list_of_port_identifiers().port_identifier();
		for (Port_identifierContext portCtx : list) {
			inputs.add(new Port("input", type, range, portCtx.getText()));
		}
	}

	@Override
	public void exitOutput_declaration(Verilog2001Parser.Output_declarationContext ctx) {
		String type = ctx.net_type() == null ? null : ctx.net_type().getText();
		String range = ctx.range_() == null ? null : ctx.range_().getText();

		List<Port_identifierContext> list = ctx.list_of_port_identifiers().port_identifier();
		for (Port_identifierContext portCtx : list) {
			outputs.add(new Port("output", type, range, portCtx.getText()));
		}
	}

	@Override
	public void exitModule_declaration(Verilog2001Parser.Module_declarationContext ctx) {
		moduleName = ctx.module_identifier().getText();
	}

}
