package hk.quantr.verilogcompiler.structure;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class Module implements Serializable{

	public String name;
	public ArrayList<Port> inputs;
	public ArrayList<Port> outputs;
	public TreeMap<String, Wire> wires;
	public LinkedHashMap<String, hk.quantr.verilogcompiler.structure.Module> modules;

	public Module() {

	}

	public Module(String name, ArrayList<Port> inputs, ArrayList<Port> outputs) {
		this.name = name;
		this.inputs = inputs;
		this.outputs = outputs;
	}

	@Override
	public String toString() {
		String str = "Module{" + "name=" + name + ", inputs=" + inputs + ", outputs=" + outputs + "}\n";
		if (inputs != null) {
			for (Port port : inputs) {
				str += "\tinput\t=\t" + port + "\n";
			}
		}
		if (outputs != null) {
			for (Port port : outputs) {
				str += "\toutput\t=\t" + port + "\n";
			}
		}
		if (wires != null) {
			for (Wire wire : wires.values()) {
				str += "\twire\t=\t" + wire + "\n";
			}
		}
		str += "}";
		return str;
	}

	public Port getPort(String name) {
		List<Port> ports = Stream.of(inputs, outputs).flatMap(x -> x.stream()).filter(e -> e.name.equals(name)).collect(Collectors.toList());
		return ports.isEmpty() ? null : ports.get(0);
	}

}
