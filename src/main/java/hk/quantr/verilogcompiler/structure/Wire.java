/*
 * Quantr Commercial License
 */
package hk.quantr.verilogcompiler.structure;

import java.io.Serializable;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class Wire implements Serializable {

	public String name;
	public String type;
	public hk.quantr.verilogcompiler.structure.Module port1Module;
	public Port port1;
	public hk.quantr.verilogcompiler.structure.Module port2Module;
	public Port port2;

	public Wire(String name, String type) {
		this.name = name;
		this.type = type;
	}

	@Override
	public String toString() {
		return "Wire{" + "name=" + name + ", type=" + type + ", port1=" + port1 + ", port2=" + port2 + '}';
	}

}
