
import hk.quantr.quantrverilogtool.antlr.Verilog2001Lexer;
import hk.quantr.quantrverilogtool.antlr.Verilog2001Parser;
import hk.quantr.verilogcompiler.listener.SingleModuleListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Collection;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestLoadAllVerilog {

	@Test
	public void test() {
		File folder = new File(System.getProperty("user.home") + "/workspace/quantr-i");
		Collection<File> files = FileUtils.listFiles(folder, new String[]{"v"}, true);
		for (File file : files) {
			try {
				System.out.println(IOUtils.toString(new FileInputStream(file), "utf-8"));
				Verilog2001Lexer lexer = new Verilog2001Lexer(CharStreams.fromString(IOUtils.toString(new FileInputStream(file), "utf-8")));
				CommonTokenStream tokenStream = new CommonTokenStream(lexer);
				SingleModuleListener listener = new SingleModuleListener();
				Verilog2001Parser parser = new Verilog2001Parser(tokenStream);
				parser.addParseListener(listener);
				Verilog2001Parser.Source_textContext context = parser.source_text();

				if (listener.moduleName == null) {
					continue;
				}
				System.out.println(listener);
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}
}
