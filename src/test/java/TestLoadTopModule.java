
import hk.quantr.peterswing.CommonLib;
import hk.quantr.verilogcompiler.VerilogCompiler;
import java.io.File;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestLoadTopModule {

	@Test
	public void test() {
//		File folder = new File("c:\\workspace\\quantr-i");
//		File topModule = new File("c:\\workspace\\quantr-i\\quantr_i.v");
		File folder = new File(System.getProperty("user.home") + "/workspace/quantr-i");
		hk.quantr.verilogcompiler.structure.Module module = VerilogCompiler.parseTopModule(folder, "quantr_i.v");
		System.out.println(CommonLib.objectToJson(module));
	}
}
