
import hk.quantr.quantrverilogtool.antlr.Verilog2001Lexer;
import hk.quantr.quantrverilogtool.antlr.VerilogMacroLexer;
import hk.quantr.quantrverilogtool.antlr.VerilogMacroParser;
import hk.quantr.verilogcompiler.macro.Macro;
import hk.quantr.verilogcompiler.macro.VerilogPreprocessor;
import java.util.ArrayList;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Token;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestCompileMacro {

	@Test
	public void test() {
		String content = "`define RESET 1'b1\n"
				+ "`define MXLEN 63:0\n"
				+ "`define INST_WIDTH 31:0\n"
				+ "`define REG_WIDTH 4:0\n"
				+ "`define ZeroWord 32'h00000000\n"
				+ "`define ZeroDWord 64'h0000000000000000\n"
				+ "`define InstMemNum 131071\n"
				+ "`define InstMemNumLog2 17\n"
				+ "//@{connect(moduleA, moduleB)} @{connect(moduleC, moduleD)}"
				+ "`define AluOpBus 7:0\n"
				+ "`define AluSelBus 2:0\n"
				+ "`define EXE_OR_OP    8'b00100101\n"
				+ "\n"
				+ "`define EXE_NOP_OP 8'b0\n"
				+ "`define EXE_RES_NOP 3'b0\n"
				+ "`define WriteDisable 1'b0\n"
				+ "`define EXE_RES_LOGIC 3'b001";
		Verilog2001Lexer lexer = new Verilog2001Lexer(CharStreams.fromString(content));
//		CommonTokenStream tokenStream = new CommonTokenStream(lexer);
//		MyVerilogListener listener = new MyVerilogListener();
//		Verilog2001Parser parser = new Verilog2001Parser(tokenStream);
//		parser.addParseListener(listener);
//		Verilog2001Parser.Source_textContext context = parser.source_text();

		for (Token token : lexer.getAllTokens()) {
			if (token.getChannel() == Verilog2001Lexer.HIDDEN && !token.getText().trim().equals("")) {
				int lineNo = token.getLine();
				int startIndex = token.getStartIndex();
				int stopIndex = token.getStopIndex();

				System.out.println(Verilog2001Lexer.VOCABULARY.getDisplayName(token.getType()) + " = " + Verilog2001Lexer.channelNames[token.getChannel()] + " = " + token.getText().replaceAll("\n", "") + " = " + token.getLine() + " , " + token.getStartIndex());
				String comment = token.getText();
				VerilogPreprocessor processor = new VerilogPreprocessor();
				ArrayList<Macro> list = processor.process(comment, lineNo, startIndex, stopIndex);
				for (Macro macro : list) {
					VerilogMacroLexer lexer2 = new VerilogMacroLexer(CharStreams.fromString(macro.command));
					CommonTokenStream tokenStream = new CommonTokenStream(lexer2);
					VerilogMacroParser parser2 = new VerilogMacroParser(tokenStream);
					parser2.compile();
					System.out.println(macro);
				}
			}
		}
	}
}
